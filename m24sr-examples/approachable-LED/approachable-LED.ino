/*The order of includes is critical for Arduino
Always include Wire, then driver, then the m24sr libraries (wrapper), and then
any NDEF headers that you need*/

#ifdef ARDUINO_SAM_DUE
 #define WIREX Wire1
#else
 #define WIREX Wire
#endif

#include <Wire.h>
#include "drv_I2C_M24SR.h"
#include "lib_wrapper.h"
#include <lib_approach_tag.h>
#include <ArduinoJson.h>

#define MANIFEST_ID "fH4pI0khRSqNEw"

#define NFC_BUFFER_SIZE 1000
#define JSON_BUFFER_SIZE 400 //manages the mapping to the nfc buffer, does not replicate content

#define SCHEMA_ID_KEY "LED Input"
#define SCHEMA_ID_KEY_OUT "LED Output"
#define SCHEMA_KEY "schema_id"
#define LED_STATUS_KEY "LED State"

static uint8_t nfc_buffer[NFC_BUFFER_SIZE] = {0};
static volatile bool nfc_msgWaiting = 0;
uint16_t status = ERROR;

static volatile uint8_t led_state = 0;

/** Initialize parameters and create JSON from startup parameters
      Caution - Routine only supports short records
returns 1 - success
0 - error
*/
uint8_t update_status() {
  uint8_t payload_length;
  uint8_t nfc_length; //length of data to be written to the m24sr
  uint8_t type_length;
  uint8_t* nfcptr;
  ApproachTagResult atr;
  uint8_t ndef_three_byte[3] = {0xD2, 0xFF, 0xFF};
  StaticJsonBuffer<JSON_BUFFER_SIZE> jsonBuffer;
  
  //initialize Node structure.
  JsonObject& root = jsonBuffer.createObject();
  root["v"] = 1;
  root["i"] = MANIFEST_ID;
  //diagnostic object
  JsonObject& diagnostic_obj = root.createNestedObject("d");
  JsonObject& status_obj = diagnostic_obj.createNestedObject("status");
  if (led_state == 1) {
    status_obj[LED_STATUS_KEY] = "On";
  } else {
    status_obj[LED_STATUS_KEY] = "Off";
  }
  
  memset((void*) nfc_buffer, 0, NFC_BUFFER_SIZE);
  payload_length = root.measureLength();
  root.printTo((char*) nfc_buffer, payload_length+1);
  
  Serial.println("\nUpdating Status to:");
  Serial.write(nfc_buffer, payload_length);
  
  atr = approachTagWrite(nfc_buffer, payload_length, NFC_BUFFER_SIZE);
  if (atr == ATR_SUCCESS) {
    memset(nfc_buffer, 0, NFC_BUFFER_SIZE);
    Serial.println("\nupdate complete");
    return 1;
  } else {
    Serial.println("\ntag write error: ");
    Serial.print(atr);
    return 0;
  }
}

/** read and validate the JSON
returns 0 - success
1 - error
*/
/*read and validate the JSON*/
uint16_t process_JSON (char* pPayload, int sPayload)
{
  StaticJsonBuffer<JSON_BUFFER_SIZE> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(pPayload);
  
  if (!root.success())
  {
    Serial.println("parseObject() failed");
    return 1;
  }
  
  if (root.containsKey(SCHEMA_KEY)) {
    
    /*If the JSON data describes the LED Schema */
    if ( !strcmp(root[SCHEMA_KEY], SCHEMA_ID_KEY) & \
      root.containsKey(LED_STATUS_KEY))
    {
      //read the LED STATUS and set LED
      if ( !strcmp(root[LED_STATUS_KEY], "Off") ) {
        led_state = 0;
        digitalWrite(LED_BUILTIN, LOW);
        Serial.println("\nLED Off");
      }
      if ( !strcmp(root[LED_STATUS_KEY], "On")) {
        led_state = 1;
        digitalWrite(LED_BUILTIN, HIGH);
        Serial.println("\nLED On");
      }
    } else {
      Serial.println("schema not recognized");
      return 0;
    }
  }
  else { 
    Serial.println("Schema ID not Found");
    return 0;
  }
  return 0;
}

void process_nfc() {
  uint16_t status = ERROR;
  ApproachTagResult atr;
  TagStructure ts = {};
  
  atr = approachTagRead((char*) nfc_buffer, &ts);
  if (atr == ATR_SUCCESS) {
    status = process_JSON(ts.payload, ts.payload_size);
  } else {
    Serial.println("tag read error: ");
    Serial.print(atr);
  }
}


void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  WIREX.begin();
  Serial.begin(115200);
  delay(500);
  while(approachTagInit()!=SUCCESS) {}
  delay(2000);
  digitalWrite(LED_BUILTIN, LOW);
  Serial.println("System Reset!");
  disableTagRF();
  update_status();
  enableTagRF();
}

void loop() {
  //yield();
  delay(1000);
  if (msgWaiting()) {
        Serial.println("New Data - disable RF!");
        delay(20);
        //disable RF
        disableTagRF();
        process_nfc();
        update_status();
        //enable RF
        Serial.println("enable RF!");
        enableTagRF();
        clearMsgWaiting();
  }
}
