/*The order of includes is critical for Arduino
Always include Wire, then driver, then the m24sr libraries (wrapper), and then
any NDEF headers that you need*/
#include <Wire.h>
#define PLAT_8266 //The ESP-8266 Platform

#include "drv_I2C_M24SR.h"
#include "lib_wrapper.h"
#include <lib_approach_tag.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>

#define GPO_INT M24SR_GPO_PIN
#define WIREX Wire
#define SDA8266 0 //D3
#define SCL8266 2 //D4

#define NFC_BUFFER_SIZE 1000
#define JSON_BUFFER_SIZE 400 //manages the mapping to the nfc buffer, does not replicate content
#define SSID_BUFFER_SIZE 25
#define PASSWORD_BUFFER_SIZE 25
#define MQTT_URL_BUFFER_SIZE 40
#define WIFI_UPDATE_DELAY 20

#define WIFI_SCHEMA_ID_KEY "ABCD1234"
#define MQTT_SCHEMA_ID_KEY "ABCD1236"
#define SSID_KEY "SSID"
#define PASS_KEY "Password"
#define URL_KEY "URL"
#define PORT_KEY "Port"
#define SCHEMA_KEY "schema_id"
#define WIFI_STATUS_KEY "Wifi Connection"

#define MQTT_TOPIC "approach/count"

static uint8_t nfc_buffer[NFC_BUFFER_SIZE] = {0};
static volatile bool nfc_msgWaiting = 0;

static char SSID_buffer[SSID_BUFFER_SIZE] = {0};
static char Password_buffer[PASSWORD_BUFFER_SIZE] = {0};
static char mqtt_server_buffer[MQTT_URL_BUFFER_SIZE] = {0};
static uint16_t port = 0;
static bool mqtt_active = false;

uint16_t status = ERROR;

WiFiClient espClient;
PubSubClient client(espClient);

/** Generage JSON Node from current wireless settings & parameters
*/
void wireless_state(JsonObject& root) {
  int wstatus = WiFi.status();
  switch (wstatus){
  case WL_NO_SHIELD:
    root[WIFI_STATUS_KEY] = "Hardware Error";
    break;
  case WL_IDLE_STATUS:
    root[WIFI_STATUS_KEY] = "Idle";
    break;
  case WL_NO_SSID_AVAIL:
    root[WIFI_STATUS_KEY] = "No SSID Available";
    break;
  case WL_SCAN_COMPLETED:
      root[WIFI_STATUS_KEY] = "Scan Complete";
    break;
  case WL_CONNECTED:
      root[WIFI_STATUS_KEY] = WiFi.SSID();
    break;
  case WL_CONNECT_FAILED:
      root[WIFI_STATUS_KEY] = "Conn Failed";
    break;
  case WL_CONNECTION_LOST:
      root[WIFI_STATUS_KEY] = "Conn Lost";
    break;
  case WL_DISCONNECTED:
      root[WIFI_STATUS_KEY] = "Disconnected";
    break;
  }
}

/** Initialize parameters and create JSON from startup parameters
      Caution - Routine only supports short records
returns 1 - success
0 - error
*/
uint8_t update_status() {
  uint8_t payload_length;
  uint8_t nfc_length; //length of data to be written to the m24sr
  uint8_t type_length;
  uint8_t* nfcptr;
  ApproachTagResult atr;
  uint8_t ndef_three_byte[3] = {0xD2, 0xFF, 0xFF};
  StaticJsonBuffer<JSON_BUFFER_SIZE> jsonBuffer;
  
  //initialize Node structure.
  JsonObject& root = jsonBuffer.createObject();
  root["v"] = 1;
  root["i"] = 123456789;
  root["o"] = 1;
  //diagnostic object
  JsonObject& diagnostic_obj = root.createNestedObject("d");
  JsonObject& status_obj = diagnostic_obj.createNestedObject("status");
  wireless_state(status_obj);
  memset((void*) nfc_buffer, 0, NFC_BUFFER_SIZE);
  payload_length = root.measureLength();
  root.printTo((char*) nfc_buffer, payload_length+1);
  
  Serial.println("\nUpdating Status to:");
  Serial.write(nfc_buffer, payload_length);
  
  atr = approachTagWrite(nfc_buffer, payload_length, NFC_BUFFER_SIZE);
  if (atr == ATR_SUCCESS) {
    memset(nfc_buffer, 0, NFC_BUFFER_SIZE);
    Serial.println("\nupdate complete");
    return 1;
  } else {
    Serial.println("tag write error: ");
    Serial.print(atr);
  }
}

void handle_Wifi_Config (const char* ssid, const char* password)
{
  Serial.println(WiFi.status());
  int wifi_timeout = 60;
  
  if (WiFi.status() == WL_CONNECTED) {
    WiFi.disconnect();
    Serial.print(WiFi.status());
    delay(500);
  }
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(WiFi.status());
    if (WiFi.status() == WL_CONNECT_FAILED){
      break;
    }
    wifi_timeout--;
    if (wifi_timeout == 0) { //if SSID is not found, the connection needs to timeout
      WiFi.disconnect();
      break;
    }
  }
}

void handle_MQTT_Connect(const char* server, uint16_t port)
{
  int connect_tries = 10;
  client.disconnect();
  client.setServer(server,port);
  while (!client.connected() & connect_tries > 0){
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ApproachableDemo")) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
      connect_tries--;
    }
  } 
  if (!client.connected()){
    Serial.println("Failed to Connect");
  }
}

/*read and validate the JSON*/
uint16_t process_JSON (char* pPayload, int sPayload)
{
  StaticJsonBuffer<JSON_BUFFER_SIZE> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(pPayload);
  
  if (!root.success())
  {
    Serial.println("parseObject() failed");
    return 1;
  }
  
  if (root.containsKey(SCHEMA_KEY)) {
    
    /*If the JSON data describes a Wifi connection, read the SSID
    and Password */
    if ( !strcmp(root[SCHEMA_KEY], WIFI_SCHEMA_ID_KEY) & \
      root.containsKey(SSID_KEY) & \
      root.containsKey(PASS_KEY) ) 
    {
      if ( strlen(root[SSID_KEY]) < SSID_BUFFER_SIZE & \
        strlen(root[PASS_KEY]) < PASSWORD_BUFFER_SIZE) 
      {
        strcpy(SSID_buffer, root[SSID_KEY]);
        strcpy(Password_buffer, root[PASS_KEY]);
        /* Connect Wifi */
        handle_Wifi_Config (SSID_buffer, Password_buffer);
      }
    }
    /*MQTT schema*/
    else if (!strcmp(root[SCHEMA_KEY], MQTT_SCHEMA_ID_KEY) & \
      root.containsKey(URL_KEY) & \
      root.containsKey(PORT_KEY) ) {
      if ( strlen(root[URL_KEY]) < MQTT_URL_BUFFER_SIZE & \
        root[PORT_KEY].as<int>() != 0) 
      {
        strcpy(mqtt_server_buffer, root[URL_KEY]);
        port = root[PORT_KEY];
        handle_MQTT_Connect(mqtt_server_buffer, port);
      }
    } else {Serial.println("schema not recognized");}
  } else { Serial.println("Schema ID not Found");}
  return 0;
}

void process_nfc() {
  uint16_t status = ERROR;
  ApproachTagResult atr;
  TagStructure ts = {};
  
  atr = approachTagRead((char*) nfc_buffer, &ts);
  if (atr == ATR_SUCCESS) {
    status = process_JSON(ts.payload, ts.payload_size);
  } else {
    Serial.println("tag read error: ");
    Serial.print(atr);
  }
}


void setup() {
  pinMode(BUILTIN_LED, OUTPUT);
  WIREX.begin(SDA8266,SCL8266);
  Serial.begin(115200);
  delay(500);
  while(approachTagInit()!=SUCCESS) {}
  delay(2000);
  Serial.println("System Reset!");
  disableTagRF();
  update_status();
  enableTagRF();
  mqtt_active = false;
}

uint8_t counter = 0;
static uint8_t mqtt_counter = 0;

void loop() {
  yield();
  delay(3000);
  mqtt_counter++;
  if (msgWaiting()) {
        Serial.println("New Data - disable RF!");
        delay(20);
        //disable RF
        disableTagRF();
        process_nfc();
        while ( counter <= WIFI_UPDATE_DELAY)
        {
          yield();
          delay(1000);
          counter++;
        }
        update_status();
        //enable RF
        Serial.println("enable RF!");
        enableTagRF();
        clearMsgWaiting();
        //memset(nfc_buffer, 0, NFC_BUFFER_SIZE);
        counter = 0;
  }
  if (WiFi.status() == WL_CONNECTED) {
    if (client.connected()){
      client.loop();
      client.publish(MQTT_TOPIC, String(mqtt_counter).c_str(), true);
      Serial.print("publishing count: ");
      Serial.println(mqtt_counter);
    } 
  }
}
