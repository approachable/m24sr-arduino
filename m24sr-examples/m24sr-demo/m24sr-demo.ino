/*The order of includes is critical for Arduino
Always include WIREX, then driver, then the m24sr libraries (wrapper), and then
any NDEF headers that you need*/
#include <Wire.h>
//#define PLAT_DUE //Arduino Due
//#define PLAT_LEO //Leonardo
#define PLAT_8266 //The ESP-8266 Platform

#include "drv_I2C_M24SR.h"
#include "arduino_m24sr_init.h"
#include "lib_wrapper.h"
#include "lib_TagType4.h"
//#include "json.h"
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>

//GPO needs to be on an interrupt, for the nucleo shield, jumper pin to another interrupt
#define GPO_INT M24SR_GPO_PIN

#ifdef PLAT_DUE
 #define ARDUINO_HEADERS
#elif defined PLAT_LEO
 #define ARDUINO_HEADERS
 #define GPO_INT 7 //Hotwire pin 7 to GPO because it can accept interrupt
#endif

// The Nucleo board lines up with SDA1 and SCL1 on the Due, so you need to call "WIREX.xxxxxx()" with all the i2c functions
#ifdef PLAT_DUE
 #define WIREX Wire1
#else
 #define WIREX Wire
#endif

// Debug channels (LEDs on X-NUCLEO board) used to capture signals with logic analyzer
 #ifdef ARDUINO_HEADERS
  #define DBG1 5
  #define DBG2 4
  #define DBG3 6
 #elif defined PLAT_8266
  #define DBG1 12 //D6 on NodeMCU
  #define DBG2 13 //D5
  #define DBG3 14 //D7
  #define SDA8266 0 //D3
  #define SCL8266 2 //D4
 #else
  ///your debug pin definitions here
 #endif

#define NFC_BUFFER_SIZE 1000
#define JSON_BUFFER_SIZE 400 //manages the mapping to the nfc buffer, does not replicate content
#define SSID_BUFFER_SIZE 25
#define PASSWORD_BUFFER_SIZE 25
#define WIFI_UPDATE_DELAY 20

#define MIMETYPE "application/approach"
#define WIFI_SCHEMA_ID_KEY "ABCD1234"
#define SSID_KEY "SSID"
#define PASS_KEY "Password"
#define SCHEMA_KEY "schema_id"
#define WIFI_STATUS_KEY "Wifi Connection"

void setDebug(int i)
{
  digitalWrite(DBG1, ((1 & i) >= 1));
  digitalWrite(DBG2, ((2 & i) >= 1));
  digitalWrite(DBG3, ((4 & i) >= 1));
}

void setupDebug()
{
  pinMode(DBG1, OUTPUT); 
  pinMode(DBG2, OUTPUT); 
  pinMode(DBG3, OUTPUT); 
}


static uint8_t nfc_buffer[NFC_BUFFER_SIZE] = {0};
//static StaticJsonBuffer<JSON_BUFFER_SIZE> jsonBuffer;//you should not use a global variable for this
static volatile bool nfc_msg_waiting = 0;

static char SSID_buffer[SSID_BUFFER_SIZE] = {0};
static char Password_buffer[PASSWORD_BUFFER_SIZE] = {0};

static bool setup_complete =false;

uint16_t status = ERROR;

/** Generage JSON Node from current wireless settings & parameters
*/
void wireless_state(JsonObject& root) {
  //modify node structure
  int wstatus = WiFi.status();
  Serial.println(wstatus);
  switch (wstatus){
  case WL_NO_SHIELD:
    root[WIFI_STATUS_KEY] = "Hardware Error";
    break;
  case WL_IDLE_STATUS:
    root[WIFI_STATUS_KEY] = "Idle";
    break;
  case WL_NO_SSID_AVAIL:
    root[WIFI_STATUS_KEY] = "No SSID Available";
    break;
  case WL_SCAN_COMPLETED:
      root[WIFI_STATUS_KEY] = "Scan Complete";
    break;
  case WL_CONNECTED:
      root[WIFI_STATUS_KEY] = WiFi.SSID();
    break;
  case WL_CONNECT_FAILED:
      root[WIFI_STATUS_KEY] = "Conn Failed";
    break;
  case WL_CONNECTION_LOST:
      root[WIFI_STATUS_KEY] = "Conn Lost";
    break;
  case WL_DISCONNECTED:
      root[WIFI_STATUS_KEY] = "Disconnected";
    break;
  }
}

/** Initialize parameters and create JSON from startup parameters
returns 1 - success
0 - error
*/
uint8_t update_status() {
  uint8_t payload_length;
  uint8_t nfc_length; //length of data to be written to the m24sr
  uint8_t type_length;
  uint8_t* nfcptr;
  uint8_t ndef_three_byte[3] = {0xD2, 0xFF, 0xFF};
  StaticJsonBuffer<JSON_BUFFER_SIZE> jsonBuffer;
  
  
  //initialize Node structure
  JsonObject& root = jsonBuffer.createObject();
  root["v"] = 1;
  root["i"] = 123456789;
  root["o"] = 1;
  //i and o as well
  JsonObject& diagnostic_obj = root.createNestedObject("d");
  JsonObject& status_obj = diagnostic_obj.createNestedObject("status");
  wireless_state(status_obj);
  memset((void*) nfc_buffer, 0, NFC_BUFFER_SIZE);
  payload_length = root.measureLength();
  type_length = strlen(MIMETYPE);
  nfc_length = payload_length + type_length + 3;
  ndef_three_byte[1] = type_length;
  ndef_three_byte[2] = payload_length;
  /*short record, no ID, 3 bytes + MIME Length + Payload Length
  the length should not exceed 256 byte for this demo. Long record is needed for 
  lengths longer than 256*/
  
  nfcptr = nfc_buffer;
  memcpy(nfcptr+1, &nfc_length, 2);//changes only second byte, good for up to 256 bytes
  nfcptr += 2;
  memcpy(nfcptr, ndef_three_byte, 3);
  nfcptr += 3;
  memcpy(nfcptr, MIMETYPE, type_length);
  nfcptr += type_length;
  root.printTo((char*) nfcptr, payload_length+1);//need to write to nfc_buffer plus offset
  
  Serial.println("\nUpdating Status to:");
  Serial.write(nfc_buffer, nfc_length+2);
  setDebug(5);
  //writing not currnetly working.
  if(OpenNDEFSession(NDEF_FILE_ID, ASK_FOR_SESSION) == SUCCESS){;
    status = NDEF_WriteNDEF(nfc_buffer);
    CloseNDEFSession(NDEF_FILE_ID);
  }
  setDebug(6);
  
  memset(nfc_buffer, 0, NFC_BUFFER_SIZE);
  Serial.println("\nupdate complete");
  return 1;
}

void handle_Wifi_Config (const char* ssid, const char* password)
{
  Serial.println(WiFi.status());
  
  if (WiFi.status() == WL_CONNECTED) {
    WiFi.disconnect();
    Serial.print(WiFi.status());
    delay(500);
  }
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(WiFi.status());
    if (WiFi.status() == WL_CONNECT_FAILED){
      break;
    }
  }
  
  
}

/*read and validate the JSON*/
uint16_t process_JSON (char* pPayload, int sPayload)
{
  StaticJsonBuffer<JSON_BUFFER_SIZE> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(pPayload);
  
  if (!root.success())
  {
    Serial.println("parseObject() failed");
    return 1;
  }
  
  if (root.containsKey(SCHEMA_KEY)) {
    
    /*If the JSON data describes a Wifi connection, read the SSID
    and Password */
    if ( !strcmp(root[SCHEMA_KEY], WIFI_SCHEMA_ID_KEY) & \
      root.containsKey(SSID_KEY) & \
      root.containsKey(PASS_KEY) ) 
    {
      if ( strlen(root[SSID_KEY]) < SSID_BUFFER_SIZE & \
        strlen(root[PASS_KEY]) < PASSWORD_BUFFER_SIZE) 
      {
        strcpy(SSID_buffer, root[SSID_KEY]);
        strcpy(Password_buffer, root[PASS_KEY]);
        /* Connect Wifi */
        handle_Wifi_Config (SSID_buffer, Password_buffer);
      }
    }
    /*Handle any other Schemas Here */
  } else {
    Serial.println("schema not recognized");
  }
  return 0;
}

void process_nfc() {
  uint16_t status = ERROR;
  sRecordInfo headerData = {0};
  sRecordInfo *pHeaderData = &headerData;
  uint32_t PayloadSize;
  const char *ndeftype = MIMETYPE;
  
  memset(nfc_buffer, 0, NFC_BUFFER_SIZE);

  if(OpenNDEFSession(NDEF_FILE_ID, ASK_FOR_SESSION) == SUCCESS){;
    setDebug(3);
    status = NDEF_ReadNDEF(nfc_buffer);
    setDebug(4);
    CloseNDEFSession(NDEF_FILE_ID);
  }
  Serial.println("\nNFCBuff 100 bytes:");
  Serial.write(nfc_buffer,100);
  Serial.println("\n");
  
  status = NDEF_IdentifySPRecord ( pHeaderData, (nfc_buffer + 2) );
  PayloadSize = ((uint32_t)(pHeaderData->PayloadLength3)<<24) | \
    ((uint32_t)(pHeaderData->PayloadLength2)<<16) | \
    ((uint32_t)(pHeaderData->PayloadLength1)<<8)  | \
    pHeaderData->PayloadLength0;

  //read and validate the type
  if (strlen(ndeftype) == pHeaderData->TypeLength) {
    if (!memcmp(ndeftype,pHeaderData->Type,pHeaderData->TypeLength)) {
      //Serial.write((char*)(nfc_buffer + 2 + pHeaderData->PayloadOffset), PayloadSize);
      status = process_JSON((char*)(nfc_buffer + 2 + pHeaderData->PayloadOffset), PayloadSize);
    } else {
      Serial.println("MIME Type does not match");
    }
  } else {
    Serial.println("bad MIME Type size");
    Serial.println (strlen(ndeftype));
    Serial.println(pHeaderData->TypeLength);
    Serial.write(pHeaderData->Type,pHeaderData->TypeLength);
  }

   memset(nfc_buffer, 0, NFC_BUFFER_SIZE);
}

void on_gpo() {
  //set msg ready
  nfc_msg_waiting = 1;
}



void setGPO () {
    
  if(OpenNDEFSession(SYSTEM_FILE_ID, ASK_FOR_SESSION) == SUCCESS){
    status = M24SR_ManageGPO(HIGH_IMPEDANCE, I2C_GPO);
    status = M24SR_ManageGPO(I2C_ANSWER_READY, RF_GPO);
    CloseNDEFSession(SYSTEM_FILE_ID);
  }
}

void setup() {
  setupDebug();
#ifdef PLAT_8266
  pinMode(BUILTIN_LED, OUTPUT);
  WIREX.begin(SDA8266,SCL8266);
#else
  WIREX.begin();
#endif
  //WIREX.setClock(50000);
  Serial.begin(9600);
  pinMode(M24SR_RFDIS_PIN, OUTPUT);
  pinMode(GPO_INT, INPUT);
  setDebug(0);
  delay(500);
  while(TT4_Init()!=SUCCESS) {}
  setDebug(1);
  delay(1);
#ifdef PLAT_8266
  yield();
#endif
  setGPO();
  attachInterrupt(digitalPinToInterrupt(M24SR_GPO_PIN), on_gpo, RISING);
  delay(2000);
#ifdef PLAT_8266
  yield();
#endif
  delay(1000);
  Serial.println("System Reset!");
  M24SR_RFConfig(0);
  update_status();
  M24SR_RFConfig(1);
}

int counter = 0;

void loop() {
#ifdef PLAT_8266
  yield();
#endif
  delay(1000);
  if (nfc_msg_waiting) {
        Serial.println("New Data - disable RF!");
        delay(20);
        //disable RF
        M24SR_RFConfig(0);
        process_nfc();
        while ( counter <= WIFI_UPDATE_DELAY)
        {
#ifdef PLAT_8266
          yield();
#endif
          delay(1000);
          counter++;
        }
        update_status();
        //enable RF
        Serial.println("enable RF!");
        M24SR_RFConfig(1);
        nfc_msg_waiting=0;
        counter = 0;
  }
}
