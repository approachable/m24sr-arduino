/* NFC.ino  02/04/2015  D.J.Whale
 *
 * Please note: This program is a hardware test program only.
 * It was written under extreme time pressure, and should only
 * be used as a reference that defines a set of sequences that
 * are known to work with the boards listed below. It is not
 * in any way a representation of good programming practice...
 * ... that takes more than a couple of hours to achieve.
 *
 * Test program for Arduino Uno to read a URI NDEF record from M24SR.
 * WIREX up a X-NUCLEO-NFC01A1 to the SCL/SCA pins.
 * Download this program and run it.
 * On the serial monitor, you should see a series of trace messages
 * for each step, plus the URL near the end that is read back from the
 * NDEF record.
 * If you get loads of 00 or FF data back, you probably don't have
 * 4.7K pullup resistors on both of SCL and SDA.
 */
 
#include <Wire.h>

#define PLAT_DUE
//#define PLAT_LEO
//#define PLAT_8266

#ifdef PLAT_DUE
 #define ARDUINO_HEADERS
#elif defined PLAT_LEO
 #define ARDUINO_HEADERS
#endif

// The Nucleo board lines up with SDA1 and SCL1 on the Due, so you need to call "WIREX.xxxxxx()" with all the i2c functions

#ifdef PLAT_DUE
 #define WIREX Wire1
#else
 #define WIREX Wire
#endif

// LEDs on X-NUCLEO board also used to capture signals with logic analyzer
 #ifdef ARDUINO_HEADERS
  #define DBG1 5
  #define DBG2 4
  #define DBG3 6
 #elif defined PLAT_8266
  #define DBG1 12 //D6 on NodeMCU
  #define DBG2 13 //D5
  #define DBG3 14 //D7
  #define SDA8266 0 //D3
  #define SCL8266 2 //D4
 #else
  ///your debug pin definitions here
 #endif

#define GETMSB(val)         ( (uint8_t) ((val & 0xFF00 )>>8) ) 
#define GETLSB(val)         ( (uint8_t) (val & 0x00FF )) 


typedef const unsigned char     uc8;
typedef const unsigned short    uc16;

void setDebug(int i)
{
  digitalWrite(DBG1, ((1 & i) >= 1));
  digitalWrite(DBG2, ((2 & i) >= 1));
  digitalWrite(DBG3, ((4 & i) >= 1));
}

void setupDebug()
{
  pinMode(DBG1, OUTPUT); 
  pinMode(DBG2, OUTPUT); 
  pinMode(DBG3, OUTPUT); 
}


#define NFC_ADDR_7BIT 0x56

void setup() 
{                
  //setupDebug();
  //WIREX.setClock(50000);
  Serial.begin(9600);
  //setDebug(0);
}

byte selectI2C[]      = {0x52};
byte selectNFCApp[]   = {0x02,0x00,0xA4,0x04,0x00,0x07,0xD2,0x76,0x00,0x00,0x85,0x01,0x01,0x00,0x35,0xC0};
byte selectCCFile[]   = {0x03,0x00,0xA4,0x00,0x0C,0x02,0xE1,0x03,0xD2,0xAF};
byte readCCLen[]      = {0x02,0x00,0xB0,0x00,0x00,0x02,0x6B,0x7D};
byte readCCFile[]     = {0x03,0x00,0xB0,0x00,0x00,0x0F,0xA5,0xA2};
byte selectNDEFFile[] = {0x02,0x00,0xA4,0x00,0x0C,0x02,0x00,0x01,0x3E,0xFD};
byte readNDEFLen[]    = {0x03,0x00,0xB0,0x00,0x00,0x02,0x40,0x79};
byte readNDEFMsg[]    = {0x02,0x00,0xB0,0x00,0x02,0x0C,0xA5,0xA7};
byte deselectI2C[]    = {0xC2,0xE0,0xB4};

void crc_test(String toprint, byte* msg, uint8_t mlength)
{
  byte crc1, crc2;
  uint16_t crc12;
  Serial.println(toprint);
  Serial.write(msg,mlength);
  Serial.print(0);
  ComputeCrc(msg, mlength-2, &crc1, &crc2);
  Serial.write(&crc1,1);
  Serial.write(&crc2,1);
  Serial.print(0);
  crc12 = M24SR_ComputeCrc(msg, mlength-2);
  Serial.write((char *)&crc12,2);
  // Serial.write((GETMSB(crc12)),1);
  // Serial.write((GETLSB(crc12)),1);
}

// The delays are required, to allow the M24SR time to process commands.

void loop()
{
  Serial.println("\nstarting");

  //select NFC app
  crc_test("selectNFCApp", selectNFCApp,sizeof(selectNFCApp));
  //select CC file
  crc_test("selectCCFile", selectCCFile,sizeof(selectCCFile));
  //readCCLen
  crc_test("readCCLen", readCCLen,sizeof(readCCLen));
  //readCCFile
  crc_test("readCCFile", readCCFile,sizeof(readCCFile));
  //selectNDEFFile
  crc_test("selectNDEFFile", selectNDEFFile,sizeof(selectNDEFFile));
  //readNDEFLen
  crc_test("readNDEFMsg", readNDEFMsg,sizeof(readNDEFMsg));
  //readNDEFMsg
  crc_test("readNDEFLen", readNDEFLen,sizeof(readNDEFLen));
  
  delay(4000);
}

word UpdateCrc(byte data, word *pwCrc)
{
  data = (data^(byte)((*pwCrc) & 0x00FF));
  data = (data^(data << 4));
  *pwCrc = (*pwCrc >> 8) 
         ^ ((word)data << 8) 
         ^ ((word)data << 3) 
         ^ ((word)data >> 4);
  return *pwCrc;
}

static uint16_t M24SR_UpdateCrc (uint8_t ch, uint16_t *lpwCrc)
{
  ch = (ch^(uint8_t)((*lpwCrc) & 0x00FF));
  ch = (ch^(ch<<4));
  *lpwCrc = (*lpwCrc >> 8)^((uint16_t)ch << 8)^((uint16_t)ch<<3)^((uint16_t)ch>>4);
    
  return(*lpwCrc);
}

word ComputeCrc(byte *data, unsigned int len, byte *crc0, byte *crc1)
{
  byte bBlock;
  word wCrc;

  wCrc = 0x6363;

  do
  {
    bBlock = *data++;
    UpdateCrc(bBlock, &wCrc);
  }
  while (--len);

  *crc0 = (byte) (wCrc & 0xFF);
  *crc1 = (byte) ((wCrc >> 8) & 0xFF);
  return wCrc;
}

static uint16_t M24SR_ComputeCrc(uc8 *Data, uint8_t Length)
{
  uint8_t chBlock;
  uint16_t wCrc;
  
  wCrc = 0x6363; // ITU-V.41
  
  do {
    chBlock = *Data++;
    M24SR_UpdateCrc(chBlock, &wCrc);
  } while (--Length);
  
  return wCrc ;
}

/* END OF FILE */
