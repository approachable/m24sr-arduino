/* NFC.ino  02/04/2015  D.J.Whale
 *
 * Please note: This program is a hardware test program only.
 * It was written under extreme time pressure, and should only
 * be used as a reference that defines a set of sequences that
 * are known to work with the boards listed below. It is not
 * in any way a representation of good programming practice...
 * ... that takes more than a couple of hours to achieve.
 *
 * Test program for Arduino Uno to read a URI NDEF record from M24SR.
 * WIREX up a X-NUCLEO-NFC01A1 to the SCL/SCA pins.
 * Download this program and run it.
 * On the serial monitor, you should see a series of trace messages
 * for each step, plus the URL near the end that is read back from the
 * NDEF record.
 * If you get loads of 00 or FF data back, you probably don't have
 * 4.7K pullup resistors on both of SCL and SDA.
 */
 
#include <Wire.h>

// #define PLAT_DUE
#define PLAT_LEO
//#define PLAT_8266

#ifdef PLAT_DUE
 #define ARDUINO_HEADERS
#elif defined PLAT_LEO
 #define ARDUINO_HEADERS
#endif

// The Nucleo board lines up with SDA1 and SCL1 on the Due, so you need to call "WIREX.xxxxxx()" with all the i2c functions

#ifdef PLAT_DUE
 #define WIREX Wire1
#else
 #define WIREX Wire
#endif

// LEDs on X-NUCLEO board also used to capture signals with logic analyzer
 #ifdef ARDUINO_HEADERS
  #define DBG1 5
  #define DBG2 4
  #define DBG3 6
 #elif defined PLAT_8266
  #define DBG1 12 //D6 on NodeMCU
  #define DBG2 13 //D5
  #define DBG3 14 //D7
  #define SDA8266 0 //D3
  #define SCL8266 2 //D4
 #else
  ///your debug pin definitions here
 #endif

void setDebug(int i)
{
  digitalWrite(DBG1, ((1 & i) >= 1));
  digitalWrite(DBG2, ((2 & i) >= 1));
  digitalWrite(DBG3, ((4 & i) >= 1));
}

void setupDebug()
{
  pinMode(DBG1, OUTPUT); 
  pinMode(DBG2, OUTPUT); 
  pinMode(DBG3, OUTPUT); 
}


#define NFC_ADDR_7BIT 0x56

void setup() 
{                
  setupDebug();
#ifdef PLAT_8266
  WIREX.begin(SDA8266,SCL8266);
#else
  WIREX.begin();
#endif
  //WIREX.setClock(50000);
  Serial.begin(9600);
  setDebug(0);
}

byte selectI2C[]      = {0x52};
byte selectNFCApp[]   = {0x02,0x00,0xA4,0x04,0x00,0x07,0xD2,0x76,0x00,0x00,0x85,0x01,0x01,0x00,0x35,0xC0};
byte selectCCFile[]   = {0x03,0x00,0xA4,0x00,0x0C,0x02,0xE1,0x03,0xD2,0xAF};
byte readCCLen[]      = {0x02,0x00,0xB0,0x00,0x00,0x02,0x6B,0x7D};
byte readCCFile[]     = {0x03,0x00,0xB0,0x00,0x00,0x0F,0xA5,0xA2};
byte selectNDEFFile[] = {0x02,0x00,0xA4,0x00,0x0C,0x02,0x00,0x01,0x3E,0xFD};
byte readNDEFLen[]    = {0x03,0x00,0xB0,0x00,0x00,0x02,0x40,0x79};
byte readNDEFMsg[]    = {0x02,0x00,0xB0,0x00,0x02,0x0C,0xA5,0xA7};
byte deselectI2C[]    = {0xC2,0xE0,0xB4};

// The delays are required, to allow the M24SR time to process commands.

void loop()
{
  Serial.println("\nstarting");

  // kill RF, select I2C
  Serial.println("selectI2C");
  setDebug(1);
  WIREX.beginTransmission(NFC_ADDR_7BIT);
  WIREX.write(selectI2C, sizeof(selectI2C));
  WIREX.endTransmission();
  delay(1);

  //select NFC app
  Serial.println("selectNFCApp");
  setDebug(2);
  WIREX.beginTransmission(NFC_ADDR_7BIT);
  WIREX.write(selectNFCApp, sizeof(selectNFCApp));
  WIREX.endTransmission();
  delay(1);
  readAndDisplay(5);
  
  //select CC file
  Serial.println("selectCCFile");
  setDebug(3);
  WIREX.beginTransmission(NFC_ADDR_7BIT);
  WIREX.write(selectCCFile, sizeof(selectCCFile));
  WIREX.endTransmission();
  delay(1);
  readAndDisplay(5);

  //readCCLen
  Serial.println("readCCLen");
  WIREX.beginTransmission(NFC_ADDR_7BIT);
  WIREX.write(readCCLen, sizeof(readCCLen));
  WIREX.endTransmission();
  delay(1);
  readAndDisplay(7);

  int len=20; //TODO get from above payload, not overly critical

  //readCCFile
  Serial.println("readCCFile");
  setDebug(4);
  WIREX.beginTransmission(NFC_ADDR_7BIT);
  WIREX.write(readCCFile, sizeof(readCCFile));
  WIREX.endTransmission();
  delay(1);
  readAndDisplay(len);
  
  
  //selectNDEFFile
  Serial.println("selectNDEFFile");
  setDebug(5);
  WIREX.beginTransmission(NFC_ADDR_7BIT);
  WIREX.write(selectNDEFFile, sizeof(selectNDEFFile));
  WIREX.endTransmission();
  delay(1);
  readAndDisplay(5);
  
  
  //readNDEFLen
  Serial.println("readNDEFLen");
  setDebug(6);
  WIREX.beginTransmission(NFC_ADDR_7BIT);
  WIREX.write(readNDEFLen, sizeof(readNDEFLen));
  WIREX.endTransmission();
  delay(1);
  
  WIREX.requestFrom(NFC_ADDR_7BIT, 7);
  int idx = 0;
  while (WIREX.available())
  {
    byte b = WIREX.read();
    Serial.print(b, HEX);
    Serial.write(' ');
    if (idx == 2) // TODO max len is 255?
    {
      len = b;
    }
    idx++;
  }
  Serial.println();
;
  //readNDEFMsg
  //len = full NDEF length, (5 byte prefix+actual URI)
  Serial.println("readNDEFMsg");
  setDebug(6);
  WIREX.beginTransmission(NFC_ADDR_7BIT);
  // Patch in the length and fix the broken CRC
  readNDEFMsg[5] = len; // this is the len returned from ReadNDEFLen
  ComputeCrc(readNDEFMsg, sizeof(readNDEFMsg)-2, &(readNDEFMsg[6]), &(readNDEFMsg[7]));
  WIREX.write(readNDEFMsg, sizeof(readNDEFMsg));
  WIREX.endTransmission();
  delay(1);

// (PCB,SW1,SW2,CRC0,CRC1)
#define PROTO_OVERHEAD 5
// 5 bytes for NDEF header (in URI format)
#define HEADER_LEN 5

  int msgStart = 1 + HEADER_LEN;
  int msgEnd   = 1 + HEADER_LEN + (len-HEADER_LEN);
  
  WIREX.requestFrom(NFC_ADDR_7BIT, len+PROTO_OVERHEAD);
  idx = 0;
  while (WIREX.available())
  {
    byte b = WIREX.read();
    if (idx >= msgStart && idx <= msgEnd)
    {
      Serial.write(b);
    }
    idx++;
  }
  Serial.println();  
  //readAndDisplay(len+PROTO_OVERHEAD);
  
  digitalWrite(DBG2, LOW); // marks end of capture region for logic analyser

  //deselectI2C
  Serial.println("deselectI2C");
  setDebug(7);
  WIREX.beginTransmission(NFC_ADDR_7BIT);
  WIREX.write(deselectI2C, sizeof(deselectI2C));
  WIREX.endTransmission();
  delay(1);
  readAndDisplay(3);  
  
  delay(2000);
}


void readAndDisplay(int len)
{
  WIREX.requestFrom(NFC_ADDR_7BIT, len);
  while (WIREX.available())
  {
    Serial.print(WIREX.read(), HEX);
    Serial.write(' ');
  }
  Serial.println();
}


word UpdateCrc(byte data, word *pwCrc)
{
  data = (data^(byte)((*pwCrc) & 0x00FF));
  data = (data^(data << 4));
  *pwCrc = (*pwCrc >> 8) 
         ^ ((word)data << 8) 
         ^ ((word)data << 3) 
         ^ ((word)data >> 4);
  return *pwCrc;
}


word ComputeCrc(byte *data, unsigned int len, byte *crc0, byte *crc1)
{
  byte bBlock;
  word wCrc;

  wCrc = 0x6363;

  do
  {
    bBlock = *data++;
    UpdateCrc(bBlock, &wCrc);
  }
  while (--len);

  *crc0 = (byte) (wCrc & 0xFF);
  *crc1 = (byte) ((wCrc >> 8) & 0xFF);
  return wCrc;
}

/* END OF FILE */

