/*This test will build off of GPO control to read the data over 
the i2c after it is written over the RF interface.*/

#include <Wire.h>
//#define PLAT_DUE //Arduino Due
//#define PLAT_LEO //Leonardo
#define PLAT_8266 //The ESP-8266 Platform

#include "drv_I2C_M24SR.h"
#include "arduino_m24sr_init.h"
#include "lib_wrapper.h"
#include "lib_TagType4.h"

//GPO needs to be on an interrupt, for the nucleo shield, jumper pin to another interrupt
#define GPO_INT M24SR_GPO_PIN



#ifdef PLAT_DUE
 #define ARDUINO_HEADERS
#elif defined PLAT_LEO
 #define ARDUINO_HEADERS
 #define GPO_INT 7 //Hotwire pin 7 to GPO because it can accept interrupt
#endif

// The Nucleo board lines up with SDA1 and SCL1 on the Due, so you need to call "WIREX.xxxxxx()" with all the i2c functions

#ifdef PLAT_DUE
 #define WIREX Wire1
#else
 #define WIREX Wire
#endif

// Debug channels (LEDs on X-NUCLEO board) used to capture signals with logic analyzer
 #ifdef ARDUINO_HEADERS
  #define DBG1 5
  #define DBG2 4
  #define DBG3 6
 #elif defined PLAT_8266
  #define DBG1 12 //D6 on NodeMCU
  #define DBG2 13 //D5
  #define DBG3 14 //D7
  #define SDA8266 0 //D3
  #define SCL8266 2 //D4
 #else
  ///your debug pin definitions here
 #endif


#define NFC_BUFFER_SIZE 1000  //BE CAREFUL not to send a message longer than this!
#define NFC_ADDR_7BIT 0x56

void setDebug(int i)
{
  digitalWrite(DBG1, ((1 & i) >= 1));
  digitalWrite(DBG2, ((2 & i) >= 1));
  digitalWrite(DBG3, ((4 & i) >= 1));
}

void setupDebug()
{
  pinMode(DBG1, OUTPUT); 
  pinMode(DBG2, OUTPUT); 
  pinMode(DBG3, OUTPUT); 
}


static volatile bool nfc_msg_waiting = 0;
static bool setup_complete =false;
static uint8_t nfc_buffer[NFC_BUFFER_SIZE] = {0};

void interrupt_response() {
  uint16_t status = ERROR;
  sRecordInfo headerData = {0};
  sRecordInfo *pHeaderData = &headerData;
  uint32_t PayloadSize;
  int offset =0;
  int toread = 60;
    
  //disable RF
  M24SR_RFConfig(0);
  delay(20);

  if(OpenNDEFSession(NDEF_FILE_ID, ASK_FOR_SESSION) == SUCCESS){;
    setDebug(3);
    status = NDEF_ReadNDEF(nfc_buffer);
    setDebug(4);
    CloseNDEFSession(NDEF_FILE_ID);
  }
  status = NDEF_IdentifySPRecord ( pHeaderData, (nfc_buffer + 2) );
  PayloadSize = ((uint32_t)(pHeaderData->PayloadLength3)<<24) | \
    ((uint32_t)(pHeaderData->PayloadLength2)<<16) | \
    ((uint32_t)(pHeaderData->PayloadLength1)<<8)  | \
    pHeaderData->PayloadLength0;

  Serial.write((nfc_buffer + 2 + pHeaderData->PayloadOffset), PayloadSize);

  //enable RF
  M24SR_RFConfig(1);
}

void on_gpo() {
  //hold execution for a few milliseconds
  //delay(10); // this may be unnecessary
  //set msg ready
  nfc_msg_waiting = 1;
}

void setup() {
   setupDebug();
#ifdef PLAT_8266
  WIREX.begin(SDA8266,SCL8266);
#else
  WIREX.begin();
#endif
  //WIREX.setClock(50000);
  Serial.begin(9600);
  pinMode(M24SR_RFDIS_PIN, OUTPUT);
  pinMode(GPO_INT, INPUT);
  
  setDebug(0);
}

uint16_t status = ERROR;

void setGPO () {
    
  if(OpenNDEFSession(SYSTEM_FILE_ID, ASK_FOR_SESSION) == SUCCESS){
    status = M24SR_ManageGPO(HIGH_IMPEDANCE, I2C_GPO);
    status = M24SR_ManageGPO(I2C_ANSWER_READY, RF_GPO);
    CloseNDEFSession(SYSTEM_FILE_ID);
  }
}

void loop() {
  if (setup_complete == false) {
    delay(500);
    setDebug(1);
    while(TT4_Init()!=SUCCESS) {}
    setDebug(2);
    delay(1);
    setGPO();
    attachInterrupt(digitalPinToInterrupt(GPO_INT), on_gpo, RISING);
    delay(100);
    setup_complete =true;
    nfc_msg_waiting=0;
  }
  if (nfc_msg_waiting) {
        delay(20);
        interrupt_response();
        delay(20);
        nfc_msg_waiting=0;
  }
#ifdef PLAT_8266
  yield();
#endif
}

